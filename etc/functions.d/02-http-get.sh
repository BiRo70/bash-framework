# Adapted from https://github.com/technomancy/leiningen and
# https://github.com/ninjudd/drip
_http_get() {
    local url="$1"
    local path="$2"
    if [[ -z $http_get ]]; then
        if type -p curl >/dev/null 2>&1; then
            http_get='curl -fL# -o'
        else
            http_get='wget -O'
        fi
    fi
    mkdir -p "$(dirname ${path})"
    echo "Downloading $url..." >&2
    eval ${http_get} "${path}.pending" "$url" || return 1
    mv -- "${path}.pending" "${path}"
}
