#!/usr/bin/env bash

# The PROJECT_ROOT is one level up from where the scripts run in the 'bin'
# directory
PROJECT_ROOT=$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )

# Load the Frameverk
. ${PROJECT_ROOT}/etc/frameverkrc

_time_from_epoch 0
_time_from_epoch xx
_time_from_epoch 1427560398

_time_to_epoch '1970-01-01_01:00:00'
_time_to_epoch '2015-03-28_16:33:18'
