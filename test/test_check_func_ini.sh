#!/usr/bin/env bash

# The PROJECT_ROOT is one level up from where the scripts run in the 'bin'
# directory
PROJECT_ROOT=$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )

CONST_COMMAND_LINE="$@"
CONST_OS_VERSION=$(uname -r)
CONST_SYSTEM_TYPE=$(uname -m)
CONST_SCRIPT_NAME=${0##*/}

# Load the Frameverk
. ${PROJECT_ROOT}/etc/frameverkrc

_ini_cfg_parser "${PROJECT_ROOT}/test/sample.ini"

for next_section in  $_ini_cfg_sections
do
    echo "Section name found $next_section"
done

cfg.section.main
echo $basemodulepath

cfg.section.master
echo $ca_name

cfg.section.agent
echo $classfile
