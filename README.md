# frameVerk

Intended to be a simple framework to help get you started in creating a shell or script based application

## Overview

    .
    ├── bin
    │   └── example_script              # A starting point for your own script
    ├── etc
    │   ├── frameverkrc                 # The environment loading RC file for frameverk
    │   └── functions.d                 # Useful utility functions that auto-load from frameverkrc
    │       ├── 00-colors.sh            #   Terminal Colors
    │       ├── 00-select-os.sh         #   Nuances for OS specifics
    │       ├── 01-log.sh               #   Helper functions for logging
    │       ├── 02-ini-config.sh        #   Helper functions for INI config file use
    │       ├── 02-network.sh           #   Helper functions for network related stuff
    │       ├── 02-time.sh              #   Helper functions for date and time
    │       ├── 02-utils.sh             #   Misc stuff with no better home
    │       ├── 03-graphite.sh          #   Helper functions for sending data to Graphite
    │       └── 03-statsd.sh            #   Helper functions for sending data to StatsD
    ├── test
    │   └── run_tests                   # Simple tests for each group of functions provided
    └── README.md


## How to use it

 - Copy or modify the example script as the starting point for the script/app you want to create
 - Make sure you modify the **Shell Behaviour** section at the top of the script to get the desired error handling in your own script

## Notes on DEFAULT_LOG vs EVENT_LOG

The intention is to have seperate output for logging and for 'events'. An event `EVENT_LOG` is intended to be something specific;

 - "User logged in"
 - "System has been restarted"

The default log `DEFAULT_LOG` is inded to capture all standard output, be it command output or information messages logged by the script.

 - Standard logging example

        _log "$LINENO" "Example test message"

 - Event logging examples

        _log_event "$LINENO" "INFO" "Example info test message"
        _log_event "$LINENO" "ERROR" "Example error test message"
        _log_event "$LINENO" "WARNING" "Example warning test message" 


## Contributing 

Published where people more talented than I can readily suggest improvements. If you see an error or something that could be better, please submit an issue.

### General Guidelines

 - **Create topic branches**. Don't ask for a pull from your master branch.

 - **One pull request per feature**. If you want to do more than one thing, send multiple pull requests.

 - **Send coherent history**. Make sure each individual commit in your pull request is meaningful. If you had to make multiple intermediate commits while developing, please squash them before sending them.

 - Fork the project, clone your fork, and configure the remotes:

        # clone your fork of the repo into the current directory in terminal
        git clone git@bitbucket.org:<your username>/frameverk.git
        # navigate to the newly cloned directory
        cd frameverk
        # assign the original repo to a remote called "upstream"
        git remote add upstream git@bitbucket.org:kxseven/frameverk.git

 - If you cloned a while ago, get the latest changes from upstream:

        # fetch upstream changes
        git fetch upstream
        # make sure you are on your 'master' branch
        git checkout master
        # merge upstream changes
        git merge upstream/master

 - Create a new topic branch to contain your feature, change, or fix:

        git checkout -b <topic-branch-name>

 - Commit your changes in logical chunks. or your pull request is unlikely be merged into the main project. Use git's interactive rebase feature to tidy up your commits before making them public.

 - Push your topic branch up to your fork:

        git push origin <topic-branch-name>

 - Open a Pull Request with a clear title and description.
